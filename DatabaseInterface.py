import sqlite3
import json
import os, sys


connection = sqlite3.connect("BooksDB.db")
cursor = connection.cursor()

def import_json_files_into_db(json_dir):
    # TODO:  Check if json_dir and db_file exists

    # Connect to DB and create cursor

    # Create Books table, if it does not already exists
    cursor.execute('''CREATE TABLE IF NOT EXISTS Books ([name] text, [author] text, [language] text, [count] int)''')

    # For every json file in json_dir, get fields
    for json_file in os.listdir(json_dir):
        if json_file.endswith(".json") and not json_file.startswith("._"):
            full_path = os.path.join(json_dir, json_file)
            record = extract_data(full_path)
            if record is None:
                insert_string = '''INSERT INTO Books (name, author, language, count) VALUES ("{}", "", "", "")'''.format(json_file)
            else:
                insert_string = '''INSERT INTO Books (name, author, language, count) VALUES ("{}", "{}", "{}", "{}")'''.format(json_file, record["author"], record["lang"], record["count"])
            cursor.execute(insert_string)

    connection.commit()

# Given a json_file as input, this function returns a dictionary of extracted fields
def extract_data(json_file) -> bool:
    f = open(json_file)
    data = f.read()
    #data.replace("\\n", "")
    try:
        extracted_data = json.JSONDecoder().decode(data)
        return extracted_data
    except:
        print("Error decoding: ", sys.exc_info()[0])
        return False
    finally:
        f.close()
    return True



def search(book: str, table: str="*", table2: str=None) -> list:
    """
    Searches for a book
    :param: book: string of type name or language of book
    :param: table: string of table ("name", "author", "lang", ("count") or (default) "*" for all)

    :return: list of output
    """

    if table2 is None: 
        ret = cursor.execute(f'''SELECT {table} FROM Books WHERE {table} Like('{book}%')''').fetchall()
    else: 
        ret = cursor.execute(f'''SELECT {table} FROM Books WHERE {table2} Like('{book}%')''').fetchall()
        
    connection.commit()

    return ret


def search_name_by_language(language: str, name: str) -> list:
    """
    Searches books by language for given name
    :param: language: string of language of book
    :param: name: string of the name of the book

    :return: the ouput as a list
    """

    ret = cursor.execute(f'''SELECT * FROM Books WHERE (language LIKE("{language}%") AND name LIKE("{name}%"))''').fetchall()
    
    connection.commit()
    return ret



def add(name: str, author: str, lang: str, count: int):
    """
    Add a new book
    :param: name: the name of the book
    :param: author: the author of the book
    :param: lang: the language of the book
    :param: count: the number of books
    """

    cursor.execute('''INSERT INTO Books (name, author, language, count) VALUES ("{}", "{}", "{}", "{}")'''
                        .format(name, author, lang, count))
    connection.commit()


# Delete a book
def delete(book: str) -> bool:
    """
    Instead of deleting the file itself,
    it is a better idea to move it into
    a 'Bought' directory.
    """

    if not os.path.exists(book):
        print("Book Doesn't exist")
        return False
    else:
        if not extract_data(book):
            print("Failed to extract data from " + book)

    if not os.path.exists("Bought"):
        os.makedirs("Bought")

    os.rename("./" + book, "./Bought/" + book)
    return True


def update(table: str, to: str):
    """
    Updates a book

    :param: table: string of table to update ("name", ("author"), ("lang"), ("count") or ("*" for all))
    :param: to: string of what to change to
    """
    cursor.execute(f'''UPDATE Books SET({table}={to})''')
    connection.commit()

# import_json_files_into_db("./DB/");
