import sys, os

from PySide6.QtGui import QGuiApplication
from PySide6.QtQml import QQmlApplicationEngine
from PySide6.QtCore import QObject, Slot, Signal

import firebase_admin
from firebase_admin import credentials
from firebase_admin import db

# __name__ == "app.window" (!= "__main__"), so
import DatabaseInterface as sdbi
# refers to ../DatabaseInterface.py


class Newsletter(QObject):
    def __init__(self):
        QObject.__init__(self)
        self.cred = credentials.Certificate("google-services.json")
    @Slot()
    def connect_firebase(self):
        firebase_admin.initialize_app(self.cred, {
            "databaseURL": "https://hindugemeinde-magazine.firebaseio.com/"
        })
        ref = db.reference('/')
        print(ref.get())

class Search(QObject):
    def __init__(self):
        QObject.__init__(self)

    SearchName = Signal(str)

    @Slot()
    def GetSearchResults(self):
        print(sdbi.search_name_by_language("English", "Bhaga"))
        self.SearchName.emit(sdbi.search_name_by_language("English", "Bhaga")[0])


newsletter = Newsletter()
view = QtDeclarative.QDeclarativeView()
context = view.rootContext()
context.setContextProperty("newsletter", newsletter)
        
app = QGuiApplication(sys.argv)
engine = QQmlApplicationEngine()

search = Search()
engine.rootContext().setContextProperty("search", search)

engine.load(os.path.join(os.path.dirname(__file__), 'main.qml'))

if not engine.rootObjects():
    sys.exit(1)

sys.exit(app.exec())
