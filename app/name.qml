import QtQuick //2.15
import QtQuick.Window //2.15
import QtQuick.Controls //2.15
import QtQuick.Controls.Material //2.15


ApplicationWindow {
    id: window2
    width: 400
    height: 600
    visible: true
    title: qsTr("RG Library")

    Material.theme: Material.System
    Material.accent: Material.Purple
}
