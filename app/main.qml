import QtQuick //2.15
import QtQuick.Window //2.15
import QtQuick.Controls //2.15
import QtQuick.Controls.Material //2.15

ApplicationWindow {
    id: window
    width: 600
    height: 1000
    visible: true
    title: qsTr("RG Library")

    Material.theme: Material.System
    Material.accent: Material.Purple

    Rectangle {
        id: welcometext
        height: 50
        color: Material.color(Material.Purple)
        
        radius: 10

        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
            margins: 5
        }

        Text {
            id: title
            text: qsTr("Welcome to RG Temple library!")
            color: "#FFFFFF"
            font.pointSize: 12

            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter

            anchors {
                verticalCenter: parent.verticalCenter
                horizontalCenter: parent.horizontalCenter
            }
        }
    }

    Image {
        id: logo
        width: 50
        height: 50
        //source: "../images/logo.jpeg"
        source: "../images/logo.png"

        anchors {
            top: welcometext.top
            left: welcometext.left
        }
    }

    Pane {
        id: mainpane
        Material.elevation: 10

        anchors {
            top: welcometext.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            margins: 0
        }

        Text {
            id: searchmethodtext
            text: qsTr("Method of search:")
            color: "#FFFFFF"
            font.pointSize: 12

            anchors {
                horizontalCenter: parent.horizontalCenter
                left: parent.left
                top: parent.top
                margins: 5
            }
        }

        Button {
            id: searchbyname
            text: qsTr("Search by name")

            anchors {
                top: searchmethodtext.bottom
                topMargin: 5
                horizontalCenter: parent.horizontalCenter
                margins: 5
            }


            onClicked: {
                //var component = Qt.createComponent("name.qml");
                //var window = component.createObject();
                //window.show();
                out_mainpane.start();
                in_namepane.start();
                title.text = "Search by Name";
            }
        }

        Button {
            id: searchbyauthor
            text: qsTr("Search by author")

            anchors {
                top: searchbyname.bottom
                horizontalCenter: parent.horizontalCenter
                margins: 5
            }

            onClicked: {
                out_mainpane.start();
                in_authorpane.start();
                title.text = "Search by Author"
            }
        }

        Button {
            id: searchnewsletter
            text: qsTr("Newsletter")

            anchors {
                top: searchnewsletter.bottom
                horizontalCenter: parent.horizontalCenter
                margins: 5
            }

            onClicked: {
                out_mainpane.start();
                in_newsletter.start();
                title.text = "Newsletter"
		newsletter.connect_firebase();
            }
        }

        Label {
            text: qsTr("Or enter a SQL query\n(table can be 'name', 'author', 'lang', ('count') or (default) '*' for all)\n e.g.: \nSELECT * FROM Books WHERE name Like('%Gita%')")
            font.pointSize: 10
            anchors {
                top: searchbyauthor.bottom
                topMargin: 10
            }
        }

        TextField {
            id: sqlquery
            placeholderText: qsTr("SQL query")
            anchors {
                top: searchbyauthor.bottom
                topMargin: 100    
            }
        }
    }

    PropertyAnimation {
        id: in_mainpane
        target: mainpane
        properties: "anchors.margins"
        from: 1000
        to: 10
        duration: 1000
        running: true
    }

    PropertyAnimation {
        id: out_mainpane
        target: mainpane
        properties: "anchors.margins"
        from: 10
        to: 1000
        duration: 1000
    }

    Pane {
        id: namepane

        Material.elevation: 10

        anchors {
            top: welcometext.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            margins: 1000
        }

        Pane {
            Material.elevation: 10
            width: 150
            height: 800

            anchors {
                top: name_back.bottom
                left: parent.left
                margins: 5
            }

            RadioButton {
                id: nameall
                checked: true
                text: qsTr("All")
            }

            RadioButton {
                id: namebengali
                text: qsTr("Bengali")
                anchors.top: nameall.bottom
            }

            RadioButton {
                id: namechinese
                text: qsTr("Chinese")
                anchors.top: namebengali.bottom
            }

            RadioButton {
                id: nameczech
                text: qsTr("Czech")
                anchors.top: namechinese.bottom
            }

            RadioButton {
                id: nameenglish
                text: qsTr("English")
                anchors.top: nameczech.bottom
            }

            RadioButton {
                id: namefinnish
                text: qsTr("Finnish")
                anchors.top: nameenglish.bottom
            }

            RadioButton {
                id: namefrench
                text: qsTr("French")
                anchors.top: namefinnish.bottom
            }

            RadioButton {
                id: namegerman
                text: qsTr("German")
                anchors.top: namefrench.bottom
            }

            RadioButton {
                id: namehindi
                text: qsTr("Hindi")
                anchors.top: namegerman.bottom
            }

            RadioButton {
                id: namehungarian
                text: qsTr("Hungarian")
                anchors.top: namehindi.bottom
            }

            RadioButton {
                id: nameitalian
                text: qsTr("Italian")
                anchors.top: namehungarian.bottom
            }

            RadioButton {
                id: namepolish
                text: qsTr("Polish")
                anchors.top: nameitalian.bottom
            }

            RadioButton {
                id: namerussian
                text: qsTr("Russian")
                anchors.top: namepolish.bottom
            }

            RadioButton {
                id: namespanish
                text: qsTr("Spanish")
                anchors.top: namerussian.bottom
            }

            RadioButton {
                id: nameswedish
                text: qsTr("Swedish")
                anchors.top: namespanish.bottom
            }

            RadioButton {
                id: nametamil
                text: qsTr("Tamil")
                anchors.top: nameswedish.bottom
            }
        }
        
        Button {
            id: name_back

            width: 25
            height: 35

            Image {
                anchors {
                    left: name_back.left
                    top: name_back.top
                    margins: 5
                }
                
                source: "../images/backicon.png"
            }
            
            anchors {
                left: namepane.left
                top: namepane.top
                margins: 5
            }

            onClicked: {
                out_namepane.start();
                in_mainpane.start();
		title.text = "Welcome to RG Temple library!"
            }
        }

        TextField {
            id: nameinput
            placeholderText: qsTr("Please enter the name of the book")
         
            anchors {
                horizontalCenter: parent.horizontalCenter
            }
        }

        Button {
            id: searchbuttonname

            width: 25
            height: 35

            Image {
                anchors {
                    left: searchbuttonname.left
                    top: searchbuttonname.top
                    topMargin: 5
                }
                
                source: "../images/searchicon.png"
            }
            
            anchors {
                left: nameinput.right
                leftMargin: 10
            }

            onClicked: search.GetSearchResults();
        }

        /*
        Pane {
            id: namesearchresult

            Label {
                id: result1
                text: ""
            }
        }
        */
    }

    PropertyAnimation {
        id: in_namepane
        target: namepane
        properties: "anchors.margins"
        from: 1000
        to: 10
        duration: 1000
    }
    
    PropertyAnimation {
        id: out_namepane
        target: namepane
        properties: "anchors.margins"
        from: 10
        to: 1000
        duration: 1000
    }
    
    Pane {
        id: authorpane

        Material.elevation: 10
        
        anchors {
            top: welcometext.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            margins: 2000
        }

        TextField {
            id: authorinput
            placeholderText: qsTr("Please enter the author of the book")
         
            anchors {
                horizontalCenter: parent.horizontalCenter
            }
        }

        Button {
            id: searchbuttonauthor

            width: 25
            height: 35

            Image {
                anchors {
                    left: searchbuttonauthor.left
                    top: searchbuttonauthor.top
                    topMargin: 5
                }
                
                source: "../images/searchicon.png"
            }
            
            anchors {
                left: authorinput.right
                leftMargin: 10
            }

            onClicked: search.GetSearchResults();
        }

        Pane {
            Material.elevation: 10
            width: 150
            height: 800

            anchors {
                top: author_back.bottom
                left: parent.left
                margins: 5
            }

            RadioButton {
                id: authorall
                checked: true
                text: qsTr("All")
            }

            RadioButton {
                id: authorbengali
                text: qsTr("Bengali")
                anchors.top: authorall.bottom
            }

            RadioButton {
                id: authorchinese
                text: qsTr("Chinese")
                anchors.top: authorbengali.bottom
            }

            RadioButton {
                id: authorczech
                text: qsTr("Czech")
                anchors.top: authorchinese.bottom
            }

            RadioButton {
                id: authorenglish
                text: qsTr("English")
                anchors.top: authorczech.bottom
            }

            RadioButton {
                id: authorfinnish
                text: qsTr("Finnish")
                anchors.top: authorenglish.bottom
            }

            RadioButton {
                id: authorfrench
                text: qsTr("French")
                anchors.top: authorfinnish.bottom
            }

            RadioButton {
                id: authorgerman
                text: qsTr("German")
                anchors.top: authorfrench.bottom
            }

            RadioButton {
                id: authorhindi
                text: qsTr("Hindi")
                anchors.top: authorgerman.bottom
            }

            RadioButton {
                id: authorhungarian
                text: qsTr("Hungarian")
                anchors.top: authorhindi.bottom
            }

            RadioButton {
                id: authoritalian
                text: qsTr("Italian")
                anchors.top: authorhungarian.bottom
            }

            RadioButton {
                id: authorpolish
                text: qsTr("Polish")
                anchors.top: authoritalian.bottom
            }

            RadioButton {
                id: authorrussian
                text: qsTr("Russian")
                anchors.top: authorpolish.bottom
            }

            RadioButton {
                id: authorspanish
                text: qsTr("Spanish")
                anchors.top: authorrussian.bottom
            }

            RadioButton {
                id: authorswedish
                text: qsTr("Swedish")
                anchors.top: authorspanish.bottom
            }

            RadioButton {
                id: authortamil
                text: qsTr("Tamil")
                anchors.top: authorswedish.bottom
            }
        }
        
        Button {
            id: author_back

            width: 25
            height: 35

            Image {
                anchors {
                    left: author_back.left
                    top: author_back.top
                    margins: 5
                }
                
                source: "../images/backicon.png"
            }
            
            anchors {
                left: namepane.left
                top: namepane.top
                margins: 5
            }

            onClicked: {
                out_authorpane.start();
                in_mainpane.start();
		title.text = "Welcome to RG Temple library!"
            }
        }

    }

    PropertyAnimation {
        id: in_authorpane
        target: authorpane
        properties: "anchors.margins"
        from: 1000
        to: 10
        duration: 1000
    }
    
    PropertyAnimation {
        id: out_authorpane
        target: authorpane
        properties: "anchors.margins"
        from: 10
        to: 1000
        duration: 1000
    }

    Pane {
        id: newsletter

        Material.elevation: 10
        
        anchors {
            top: welcometext.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            margins: 1000
        }
        
        Button {
            id: news_back

            width: 25
            height: 35

            Image {
                anchors {
                    left: news_back.left
                    top: news_back.top
                    margins: 5
                }
                
                source: "../images/backicon.png"
            }
            
            anchors {
                left: namepane.left
                top: namepane.top
                margins: 5
            }

            onClicked: {
                out_newsletter.start();
                in_mainpane.start();
		title.text = "Welcome to RG Temple library!"
	    }
        }

    }

    PropertyAnimation {
        id: in_newsletter
	target: newsletter
	properties: "anchors.margins"
	from: 100
	to: 10
	duration: 500
    }

    PropertyAnimation {
        id: out_newsletter
	target: newsletter
	properties: "anchors.margins"
	from: 10
	to: 500
	duration: 1000
    }

    signal reconnect(string results)
    Component.onCompleted: search.GetSearchResults().connect(reconnect)
    Connections {
        target: window
        onReconnect: {
            result1.text = results
        }
    } 
}
